package be.kdg.patterns;

import java.util.Objects;

/**
 * Hier mag je in principe niets wijzigen!
 */
public class Product {
    private int id;
    private String omschrijving;
    private double prijs;
    private int categorieId; // foreign key naar gerelateerd categorie-record

    public Product(String omschrijving, double prijs) {
        this(-1, omschrijving, prijs, -1);
    }

    public Product(String omschrijving, double prijs, int categorieId) {
        this(-1, omschrijving, prijs, categorieId);
    }

    // Bewust niet public, alleen opgeroepen vanuit DAO klasse.
    Product(int id, String omschrijving, double prijs, int categorieId) {
        this.id = id;
        this.omschrijving = omschrijving;
        this.prijs = prijs;
        this.categorieId = categorieId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setPrijs(double prijs) {
        this.prijs = prijs;
    }

    public int getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(int categorieId) {
        this.categorieId = categorieId;
    }

    public String toString() {
        return String.format("product[%2d, %-7s, €%.2f, cat: %d]", id, omschrijving, prijs, categorieId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
