package be.kdg.patterns;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;

/**
 * Deze klasse erft over van HsqlDao en kan dus de methoden
 * van de superklasse oproepen.
 *
 * De meeste methoden zijn klaar, maar een aantal moet je nog uitwerken.
 */
public class CategorieDao extends HsqlDao {
    private ProductDao productDao;

    public CategorieDao(ProductDao productDao) {
        this.productDao = productDao;
        Connection connection = maakConnectie();
        Statement statement = maakStatement(connection);
        maakTabel(statement);
        sluitStatementEnConnectie(statement, connection);
    }

    private void maakTabel(Statement statement) {
        try {
            statement.execute("CREATE TABLE categorieen (id INTEGER IDENTITY, naam CHAR(20))");
        } catch (SQLException e) {
            // no problem, table already exists
        }
    }

    public boolean clear() {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = maakConnectie();
            statement = maakStatement(connection);
            statement.execute("DROP TABLE categorieen IF EXISTS");
            maakTabel(statement);
            sluitStatementEnConnectie(statement, connection);
            return true;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    /**
     * WERK UIT
     * Deze methode schrijft een nieuw categorie-record weg naar de database.
     * Alleen het toevoegen van de gerelateerde producten moet jij nog doen.
     *
     * @param categorie het toe te voegen categorie-object
     * @return true indien succesvol toegevoegd
     */
    public boolean create(Categorie categorie) {
        Connection connection = null;
        Statement statement = null;
        try {
            if (categorie.getId() >= 0) return false;
            connection = maakConnectie();
            statement = maakStatement(connection);
            String naam = categorie.getNaam();
            int rowsAffected = statement.executeUpdate("INSERT INTO categorieen VALUES (NULL, '" + naam + "')");
            if (rowsAffected != 1) {
                sluitStatementEnConnectie(statement, connection);
                return false;
            }
            ResultSet resultSet = statement.executeQuery("CALL IDENTITY()");
            if (!resultSet.next()) {
                sluitStatementEnConnectie(statement, connection);
                return false;
            }
            int id = resultSet.getInt(1);
            categorie.setId(id);
            sluitStatementEnConnectie(statement, connection);

            //Voeg nu alle producten van deze categorie toe aan de database. Gebruik daarvoor het productDao-object.
            //Vergeet niet om eerst van elk product de setCategorieId op te roepen!
            boolean success = true;
            //...
            return success;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    /**
     * WERK UIT
     * Deze methode wijzigt een categorierecord van naam en doet een update:
     * Alle gerelateerde producten worden eerst uit de database verwijderd, dan worden de nieuwe producten
     * van deze categorie weer toegevoegd.
     *
     * @param categorie de te wijzigen categorie
     * @return true indien de wijziging succesvol verliep
     */
    public boolean update(Categorie categorie) {
        Connection connection = null;
        Statement statement = null;
        try {
            if (categorie.getId() < 0) return false;
            connection = maakConnectie();
            statement = maakStatement(connection);
            int id = categorie.getId();
            String naam = categorie.getNaam();
            statement.executeUpdate("UPDATE categorieen SET naam = '" + naam + "' WHERE id=" + id);
            sluitStatementEnConnectie(statement, connection);
            Set<Product> producten = categorie.getProducten();
            Set<Product> oudeProducten = productDao.retrieveByCategorie(id);

            // wis de producten die verwijderd werden
            //...

            // update producten; pas de categorieId aan en voeg dan de nieuwe producten toe
            boolean success = true;
           //...
            return success;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    public Categorie retrieve(int id) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = maakConnectie();
            statement = maakStatement(connection);
            ResultSet resultSet = statement.executeQuery("SELECT naam FROM categorieen WHERE id=" + id);
            if (!resultSet.next()) {
                sluitStatementEnConnectie(statement, connection);
                return null;
            }
            String naam = resultSet.getString("naam");
            Categorie categorie = new Categorie(id, naam);
            sluitStatementEnConnectie(statement, connection);
            // gerelateerde producten ophalen:
            Set<Product> producten = productDao.retrieveByCategorie(id);
            categorie.setProducten(producten);
            return categorie;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return null;
        }
    }

    /**
     * WERK UIT
     * Deze methode verwijdert een categorierecord uit de database én alle gerelateerde producten.
     * @param id de te verwijderen categorie
     * @return true indien succesvol verwijderd
     */
    public boolean delete(int id) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = maakConnectie();
            statement = maakStatement(connection);
            int rowsAffected = statement.executeUpdate("DELETE FROM categorieen WHERE id = " + id);
            if (rowsAffected != 1) {
                sluitStatementEnConnectie(statement, connection);
                return false;
            }
            sluitStatementEnConnectie(statement, connection);

            // wis alle producten van deze categorie
            //...
            return true;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }
}
